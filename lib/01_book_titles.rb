class Book
  # TODO: your code goes here!
  attr_accessor :title

  def title
    title_words = @title.split
    except = 'a an and at in of the to but'

    title_words.each do |wrd|
      wrd.capitalize! unless except.include?(wrd)
      wrd.capitalize! if wrd == title_words.first || wrd == 'i'
    end

    title_words.join(' ')
  end
end
