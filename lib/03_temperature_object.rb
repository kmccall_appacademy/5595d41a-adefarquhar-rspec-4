class Temperature
  # TODO: your code goes here!

  attr_accessor :options

  def initialize(options = {})
    @defaults = { c: nil, f: nil }
    @options = @defaults.merge(options)
  end

  def in_fahrenheit
    return options[:f] unless options[:f].nil?
    options[:c] * 1.8 + 32
  end

  def in_celsius
    return options[:c] unless options[:c].nil?
    ((options[:f] - 32) / 1.8).round
  end

  def self.from_celsius(temp)
    Temperature.new(c: temp)
  end

  def self.from_fahrenheit(temp)
    Temperature.new(f: temp)
  end
end

class Celsius < Temperature
  def initialize(temp)
    @temp = temp
  end

  def in_celsius
    @temp
  end

  def in_fahrenheit
    @temp * 1.8 + 32
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    @temp = temp
  end

  def in_fahrenheit
    @temp
  end

  def in_celsius
    ((@temp - 32) / 1.8).round
  end
end
