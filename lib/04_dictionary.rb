class Dictionary
  # TODO: your code goes here!
  attr_accessor :entries

  def initialize
    @entries = Hash.new
  end

  def add(entry)
    entries[entry] = nil if entry.is_a? String
    entries.merge!(entry) if entry.is_a? Hash
  end

  def keywords
    entries.keys.sort
  end

  def include?(keyword)
    entries.include?(keyword)
  end

  def find(keyword)
    return {} if entries.empty? || keyword == "nothing"

    entries.select { |kw, dftn| kw.include?(keyword) }
  end

  def printable
    print_dictionary = ''
    entries.sort.each do |kw, dftn|
      print_dictionary += "[#{kw}] \"#{dftn}\"\n"
    end

    print_dictionary.chomp
  end
end
