class Timer
  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    secs_left = seconds

    hr, secs_left = convert(secs_left) if secs_left > 3600
    min, secs_left = convert(secs_left) if secs_left > 60
    sec = padded(secs_left)

    hr = '00' if hr.nil?
    min = '00' if min.nil?

    "#{hr}:#{min}:#{sec}"
  end

  def convert(secs_left)
    if secs_left > 3600
      hr = secs_left / 3600
      secs_left = secs_left % 3600
      return padded(hr), secs_left
    elsif secs_left > 60
      min = secs_left / 60
      secs_left = secs_left % 60
      return padded(min), secs_left
    end
  end

  def padded(digits)
    return digits.to_s if digits > 9
    "0#{digits}"
  end
end
